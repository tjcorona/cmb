
.. index:: Info Tab

The Info Tab
============

This tab contains no configurable options; There are purely information items on this tab.

The “Data Hierarchy" window has a hierarchical list of all the elements in the scene. Each can be individually selected to view more information.

.. todo::
	Expand

